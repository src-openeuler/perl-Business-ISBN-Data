%define mod_name Business-ISBN-Data
Name:           perl-%{mod_name}
Version:        20250226.001
Release:        1
Summary:        Data pack for Business::ISBN
License:        Artistic-2.0
URL:            https://metacpan.org/pod/Business::ISBN::Data
Source0:        https://cpan.metacpan.org/authors/id/B/BR/BRIANDFOY/%{mod_name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  coreutils findutils make perl-interpreter perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
# Test Suite
BuildRequires:  perl(Test::More) perl(Test::Pod) perl(Test::Pod::Coverage)

%description
This package is the data pack for Business::ISBN

%package_help

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc Changes README.pod examples/ t/
%{perl_vendorlib}/Business/

%files help
%{_mandir}/man3/Business::ISBN::Data.3*

%changelog
* Thu Feb 27 2025  openeuler_bot <infra@openeuler.sh> - 20250226.001-1
- data update for 2025-02-26 (version:20250226.001)

* Sat Feb 22 2025  openeuler_bot <infra@openeuler.sh> - 20250220.001-1
- Changes for version 20250220.001 - 2025-02-20
  - data update for 2025-02-19

* Wed Feb 12 2025 sqfu <dev01203@linx-info.com> - 20250205.001-1
- update version to 20250205.001
- Business ISBN Data update for 2025-02-05

* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 20241025.001-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Nov 04 2024 zhangxianjun <zhangxianjun@kylinos.cn> - 20241025.001-1
- update to version 20241025.001
- Business ISBN Data update for 2024-10-25

* Mon May 06 2024 wangshuo <wangshuo@kylinos.cn> - 20240426.001-1
- update to version 20240426.001

* Fri Mar 22 2024 wangshuo <wangshuo@kylinos.cn> - 20240321.001-1
- update version to 20240321.001

* Thu Jan 25 2024 zhangyao <zhangyao108@huawei.com> - 20231220.001-1
- update version to 20231220.001

* Wed Jul 26 2023 zhangyao <zhangyao108@huawei.com> - 20230719.001-1
- update version to 20230719.001

* Mon Oct 31 2022 hongjinghao <hongjinghao@huawei.com> - 20210112.006-2
- use %{mod_name} marco

* Mon Dec 20 2021 shixuantong <shixuantong@huawei.com> - 20210112.006-1
- update version to 20210112.006

* Mon Nov 22 2021 shixuantong <shixuantong@huawei.com> - 20191107-3
- fix url error in spec file 

* Tue Feb 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 20191107-2
- Modify the files

* Mon Feb 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 20191107-1
- Package init
